# iCloud Photos Downloader 

- Refactored to use `Python 3.11`
- This is a fork of [icloud_photos_downloader](https://github.com/icloud-photos-downloader/icloud_photos_downloader)
- refer to [issue 425](https://github.com/icloud-photos-downloader/icloud_photos_downloader/issues/425)
- deleted all tests and docker support
- this project is for my personal use, you can clone and fork as you wish to take advantage of the refactor to `Python 3.11`
- `Python 3.10` was causing kernel panic in MacOS Monterrey and Ventura, upgraded to `Python 3.11`


# History of changes

## 31 Oct 2022
- no code changes, using Python 3.11.0 due to kernel panic in MacOS Monterrey and Ventura with 3.10

## 23 May 2022
- deleted this code from module `icloudpd/base.py` function `download_photo()` as `Photo` object does not have `item_type` attribute/property 
```commandline
        if skip_videos and photo.item_type != "image":
            logger.set_tqdm_description(
                "Skipping %s, only downloading photos." % photo.filename
            )
            return
        if photo.item_type != "image" and photo.item_type != "movie":
            logger.set_tqdm_description(
                "Skipping %s, only downloading photos and videos. "
                "(Item type was: %s)" % (photo.filename, photo.item_type)
            )
            return
```
- replaced `pyicloud_ipd` for `pyicloud` in the following modules:
  - icloudpd/authentication.py
  - icloudpd/base.py
  - icloudpd/download.py
- requirements.txt replaced `pyicloud_ipd==0.10.1` for `pyicloud==1.0.0`
- working output sanitized:
``` commandline
python icloudpd.py --username appleid@example.com --password 12345678 --directory /Volumes/photos    
2022-04-19 21:24:40 DEBUG    Authenticating...
2022-04-19 21:24:41 DEBUG    Looking up all photos and videos from album All Photos...    
2022-04-19 21:24:41 INFO     Downloading 123724 original photos and videos to /Volumes/photos ...
/Volumes/photos/IMG_0000.JPG already exists.: 100%|#####################################################################################| 123724/123724 [1:27:33<00:00, 23.55it/s]
2022-04-19 22:52:14 INFO     All photos have been downloaded!
```
